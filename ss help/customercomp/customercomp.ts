import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
    selector : 'my-signup',
    templateUrl : './customer.component.html'
})

export class CustomerComponent implements OnInit {

    customerForm : FormGroup;
    //customer: Customer = new Customer(); //irrid namel il class

    constructor(private fb: FormBuilder) {}

    ngOnInit() : void {
        //initialising
        this.customerForm = this.fb.group({
            firstName:['', [Validators.required, Validators.minLength(3)]],
            lastName:['', [Validators.required, Validators.maxLength(20)]],
            email:['', [Validators.required,Validators.pattern('[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+')]]
        });
    }


    save(): void {
        console.log('Saved: ' + JSON.stringify(this.customerForm.value));
    }
}