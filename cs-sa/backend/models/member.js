var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var memberSchema = new Schema ({
    id: String,
    name: String,
    surname: String,
    dob: String,
    nationality: String,
    gender : String,
    email : String,
    mobile: String
})

module.exports = mongoose.model('member',memberSchema, 'members')