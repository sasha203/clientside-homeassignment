var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var articleSchema = new Schema ({
    title: String,
    content: String,
    image: String
})

module.exports = mongoose.model('article',articleSchema, 'articles')