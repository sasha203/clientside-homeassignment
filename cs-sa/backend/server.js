var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cors = require('cors');
var app = express();

var User = require('./models/user.js');
var Article = require('./models/article.js');
var Member = require('./models/member.js');


app.use(cors());
app.use(bodyParser.json());

mongoose.connect('mongodb://admin:a12345@ds157864.mlab.com:57864/cs-db', {useMongoClient:true}, err=>{
    if(!err){
        console.log('connected to mongoDB');
    }
    else {
        console.log("Error: " + err); 
    }
});

app.listen(3000)

app.get('/',(req,res) =>{
    res.send("Server Root");
})


app.post('/register',(req,res) =>{
   // res.header("Access-Control-Allow-Origin", "*");
    var userData = req.body;
    var user = new User(userData);
    user.save((err, result) => {
        if(err) {
            console.log('saving user error');
        } else {
            res.sendStatus(200).send(result);
            console.log('Data Entered');
        }    
    })
})


app.post('/articles',(req,res) =>{
    //res.header("Access-Control-Allow-Origin", "*");
    var articleData = req.body;
    var article = new Article(articleData);
    article.save((err, result) => {
        if(err) {
            console.log('saving article error');
        } else {
            res.sendStatus(200).send(result);
            console.log('article Entered');
        }    
    })
})

app.post('/members',(req,res) =>{
    var memberData = req.body;
    var member = new Member(memberData);
    member.save((err, result) => {
        if(err) {
            console.log('saving member error');
        } else {
            res.sendStatus(200).send(result);
            console.log('member Entered');
        }    
    })
})

//login
app.post('/admin', (req, res) => {
    let userData = req.body
    //compares login email with that of the db.
    User.findOne({email: userData.email}, (error, user) => {
        if(error) {
            console.log("error");
        }
        else {
            if(!user) {
                res.status(401).send('Invalid email');
            }
            else {
                if(user.password != userData.password) {
                    res.status(401).send('Invalid password');
                }
                else {
                    res.status(200).send(user);
                }
            }
        }
    })
})