import { Component, OnInit } from '@angular/core';
import { IArticle } from '../article/article';
import { ApiService } from '../api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'latest-news',
  templateUrl: './latestNews.html',
})

export class LatestNewsComponent implements OnInit {
  title = 'Latest News';
  errorMessage: any;
  articles: IArticle [];

  constructor(private _apiService: ApiService, private route: ActivatedRoute,
    private router: Router){}

  ngOnInit(){
      console.log("subscribing articles");
      this._apiService.getArticles()
        .subscribe(articles => {
          this.articles = articles;
        },
        error => this.errorMessage = <any>error);
  }

  onBack(): void {
    this.router.navigate(['/articles']);
  }

}
