import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Login} from '../admin/login'
import { ApiService } from '../api.service';

@Component({
  selector: 'admin',
  templateUrl: './admin.html',
})

export class AdminComponent implements OnInit{
    title = 'Admin';
    loginForm : FormGroup;
    user: Login = new Login();
    statusCode: Response;
    errorMessage: any;

    constructor(private _apiService: ApiService, private fb: FormBuilder) { }


    ngOnInit() :void {
        this.loginForm = this.fb.group({
            email:['', [Validators.required,Validators.minLength(4),Validators.pattern('[a-zA-Z._%+-]+@[a-zA-Z0-9.-]+')]],
            pass:['', [Validators.required]]
        });
    }

    //login.
    log(): void{
        this._apiService.postLoginData(this.loginForm.value) 
        .subscribe(
          (successCode) => this.statusCode = successCode,
          (error: any) => this.errorMessage = <any>error
      )}

  
  
}
