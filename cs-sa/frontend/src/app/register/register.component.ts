import { Component, OnInit } from '@angular/core';

import { Registration } from './registration';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
  selector: 'register',
  templateUrl: './register.component.html'
})
export class RegisterComponent{
  title : String = "Register";
  registerForm: FormGroup;
  user: Registration = new Registration();
  statusCode: Response;
  errorMessage: any;
 
  //registerData: Registration[] = [];
  constructor(private _apiService: ApiService, private fb:FormBuilder){
    this.registerForm = fb.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
  });
  }
  
    post(): void{
      //console.log(this.registerData);
      this._apiService.postRegData(this.registerForm.value)
      .subscribe(
        (successCode) => this.statusCode = successCode,
        (error: any) => this.errorMessage = <any>error
    )}
}