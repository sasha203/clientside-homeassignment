import { Component, OnInit } from '@angular/core';
import { IMember } from '../member/member';
import { ApiService } from '../api.service';


@Component({
    selector: 'search-members',
    templateUrl: './searchMembers.html'
})

export class SearchMembersComponent implements OnInit {
    title = "Member Search"
    _listFilter = '';
    errorMessage = '';
    filteredMembers: IMember[] = [];
    members: IMember[] = [];
  
    constructor(private _apiService: ApiService) {}

    get listFilter(): string {
        return this._listFilter;
    }

    set listFilter(value: string) {
        this._listFilter = value;
        this.filteredMembers = this.listFilter ? this.performFilter(this.listFilter) : this.members;
    }

    performFilter(filterBy: string): IMember[] {
        filterBy = filterBy.toLocaleLowerCase();
        return this.members.filter((member: IMember) =>
            member.name.toLocaleLowerCase().indexOf(filterBy) !== -1 ||
            member.surname.toLocaleLowerCase().indexOf(filterBy) !== -1 ||
            member.id.toLocaleLowerCase().indexOf(filterBy) !== -1);
    }

    ngOnInit(): void {
        this._apiService.getMembers().subscribe(
            members => {
                this.members = members;
                this.filteredMembers = this.members;
            },
            error => this.errorMessage = <any>error
        );
    }
}