import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ApiService} from '../api.service';

@Component({
    selector: 'member',
    templateUrl: './member.html',
  })



  export class MemberComponent implements OnInit {
    title = 'Personal Info';
    memberForm : FormGroup;
    statusCode: Response;
    errorMessage: any;
  
    constructor(private _apiService: ApiService, private fb: FormBuilder) { }
  
    ngOnInit() :void {
        this.memberForm = this.fb.group({
            id:['', [Validators.required]],
            name:['', [Validators.required,Validators.minLength(4),,Validators.maxLength(20)]],
            surname:['', [Validators.required,Validators.minLength(4),,Validators.maxLength(20)]],
            dob:['', [Validators.required]],
            nationality:['', [Validators.required]],
            gender:['', [Validators.required]],
            email: ['', [Validators.required, Validators.pattern('[a-zA-Z._%+-]+@[a-zA-Z0-9.-]+')]],
            mobile: ['', [Validators.required, Validators.pattern('^[0-9]+$')]]
        });
    }

 
      post() : void {
        console.log("Personal Data Added");
        this._apiService.postMemberData(this.memberForm.value)
        .subscribe(
          (successCode) => this.statusCode = successCode,
          (error: any) => this.errorMessage = <any>error
    )}

  }