export interface IMember {
    id: String;
    name: String;
    surname: String;
    dob: String;
    nationality: String;
    gender : String;
    email : String;
    mobile: String;
}