import { Component, OnInit  } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'contact-us',
  templateUrl: './contactUs.html'
})

export class ContactUsComponent implements OnInit  { 
  title = 'Contact Us';
  contactForm : FormGroup;
 

 
  constructor(private fb: FormBuilder) { }

  ngOnInit() :void {
    this.contactForm = this.fb.group({
      email: ['', [Validators.required, Validators.pattern('[a-zA-Z._%+-]+@[a-zA-Z0-9.-]+')]],
      mobile: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      message:['', [Validators.required]]
    });

  }


 

  save() : void {
      console.log('Message Successfully submitted!\n' + 'Email: ' + 
        JSON.stringify(this.contactForm.value.email) +
        '\nMobile: ' +  JSON.stringify(this.contactForm.value.mobile) +
        '\nMessage: ' +  JSON.stringify(this.contactForm.value.message)  
      );


  }


}
