import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ApiService} from '../api.service';

@Component({
    selector: 'article',
    templateUrl: './article.html',
  })



  export class ArticleComponent implements OnInit {
    pageTitle = 'Add an Article';
    articleForm :FormGroup;
    statusCode: Response;
    errorMessage: any;
    
  
  
    constructor(private _apiService: ApiService, private fb: FormBuilder) { }
  


    ngOnInit() : void {
        this.articleForm= this.fb.group({
            title : ['', [Validators.required]],
            content : ['', [Validators.required]],
            image : ['', [Validators.required]]
        });

    }

    post() : void {
        console.log("Article Added!");
        this._apiService.postArticleData(this.articleForm.value)
        .subscribe(
          (successCode) => this.statusCode = successCode,
          (error: any) => this.errorMessage = <any>error
    )}

  }