export interface IArticle {
    title: String;
    content: String;
    image: String;
}