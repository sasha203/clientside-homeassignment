import { Component } from '@angular/core';

@Component({
  selector: 'about-us',
  templateUrl: './aboutUs.html'
})

export class AboutUsComponent {
  title = 'About Us';
}
