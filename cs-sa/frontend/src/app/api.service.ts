import {HttpClient, HttpErrorResponse} from '@angular/common/http'
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs'
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { Registration } from './register/registration';
import {Login} from './admin/login'
import { IArticle } from './article/article';
import { IMember } from './member/member';
import { tap, catchError } from 'rxjs/operators';


@Injectable()
export class ApiService{
    articles = [];
    members = [];

    constructor (private http: HttpClient){}

    postRegData(userData: Registration ):Observable<Response>{
        console.log(userData.email + ' ' + userData.password);
        return this.http.post('http://localhost:3000/register', userData, {responseType: 'text'})
        .do(data => console.log('All: ' + JSON.stringify(data)))
        .catch(this.handleError);
    }

    postArticleData(articleData: IArticle ):Observable<Response>{
        console.log(articleData.title + " was added");
        return this.http.post('http://localhost:3000/articles', articleData, {responseType: 'text'})
        .do(data => console.log('All: ' + JSON.stringify(data)))
        .catch(this.handleError);
    }

    postMemberData(memberData: IMember ):Observable<Response>{
        console.log(memberData.name + " was added");
        return this.http.post('http://localhost:3000/members', memberData, {responseType: 'text'})
        .do(data => console.log('All: ' + JSON.stringify(data)))
        .catch(this.handleError);
    }


    getArticles(){
        console.log("Getting articles");
        return this.http.get<IArticle[]>('http://localhost:3000/articles')
        .do(data => console.log('All: ' + JSON.stringify(data)))
        .catch(this.handleError);
    }
    
    /*
    //will use this if db method won't work.
    private memberUrl = 'api/members/members.json'; 
    getMembers(): Observable<IMember[]> {
        return this.http.get<IMember[]>(this.memberUrl).pipe(
          tap(data => console.log('All: ' + JSON.stringify(data))),
          catchError(this.handleError)
        );
      }
    */
    
    //db version. 
    getMembers(){
        console.log("Getting Members");
        return this.http.get<IMember[]>('http://localhost:3000/members')
        .do(data => console.log('All: ' + JSON.stringify(data)))
        .catch(this.handleError);
    }
    
    //admin
    postLoginData(userData: Login ):Observable<Response>{
        console.log(userData.email + ' ' + userData.password);
        return this.http.post('http://localhost:3000/admin', userData, {responseType: 'text'})
        .do(data => console.log('All: ' + JSON.stringify(data)))
        .catch(this.handleError);
    }


    private handleError(err: HttpErrorResponse) {
        console.error(err.message); 
        return Observable.throw(err.message); 
    }
}
