import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './api.service';
import {MatButtonModule, MatToolbarModule, MatInputModule} from '@angular/material';


import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


import {MatListModule} from '@angular/material/list';
import { RegisterComponent } from './register/register.component';
import { AboutUsComponent } from './aboutUs/aboutUs.component';
import { AdminComponent } from './admin/admin.component';
import { ArticleComponent } from './article/article.component';
import { ContactUsComponent } from './contactUs/contactUs';
import { LatestNewsComponent } from './latestNews/latestNews';
import { MemberComponent } from './member/member.component';
import { SearchMembersComponent } from './searchMembers/searchMembers';


@NgModule({
  declarations: [
    AppComponent, AboutUsComponent, AdminComponent, ArticleComponent, 
    ContactUsComponent,LatestNewsComponent, MemberComponent, SearchMembersComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, FormsModule, 
    ReactiveFormsModule, MatButtonModule, MatInputModule, 
    MatToolbarModule, BrowserAnimationsModule,NoopAnimationsModule, MatListModule, 
    
    RouterModule.forRoot(
      [
        {path: '', redirectTo: '/latestNews', pathMatch: 'full'},
        { path: 'register', component: RegisterComponent},
        
        { path: 'latestNews', component: LatestNewsComponent },
        { path: 'aboutUs', component: AboutUsComponent },
        { path: 'contactUs', component: ContactUsComponent },
        { path: 'admin', component: AdminComponent },
      
        { path: 'members', component: MemberComponent },
        { path: 'articles', component: ArticleComponent },
        { path: 'searchMembers', component: SearchMembersComponent } 
      ]
    )
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
